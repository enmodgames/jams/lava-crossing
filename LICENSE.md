# This project licensed as [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, [Enlightened Modifications](https://enmod.itch.io/) has waived all copyright and related or neighboring rights to this "Lava Crossing" Godot Engine project. This work is published from: United States. 